//import express module
var express = require('express');

//create the express module
const app = express();

//server port
var port = 8080;

app.use(express.static("./index"));

app.get("/", function (req, res) {
	res.sendFile(__dirname + "/" + "/index.index.html");
})

var server = app.listen(port, function() {
	var host = server.address().address
	var port = server.address().port
	
	console.log("UI server listening at http://%s:%s", host, port)
})